const mongoose = require('mongoose')


const uri = process.env.MONGODB_URI || 'mongodb://localhost/databasetest'



mongoose.connect(uri,{
    useNewUrlParser:true,
    useCreateIndex:true
})

const connection = mongoose.connection
connection.once('open',()=>{
    console.log(`database is connected`)
})